<?php
/**
 * @file
 *   Contains a view handler for displaying mention entity labels.
 */

class entity_mentions_views_handler_field_mentioned extends entity_mentions_views_handler_field_entity {

  function construct() {
    parent::construct();

    $this->define_additional_fields('mentioned');
  }
}
