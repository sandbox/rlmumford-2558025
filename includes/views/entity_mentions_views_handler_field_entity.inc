<?php
/**
 * @file
 *   Contains a view handler for displaying mention entity labels.
 */

class entity_mentions_views_handler_field_entity extends views_handler_field {

  function construct() {
    parent::construct();

    $this->define_additional_fields();
  }

  function define_additional_fields($type = 'entity') {
    $this->additional_fields['entity_type'] = array(
      'field' => $type . '_type',
    );
    $this->additional_fields['entity_id'] = array(
      'field' => $type . '_id',
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $entity_type = $values->{$this->aliases['entity_type']};
    $id = $values->{$this->aliases['entity_id']};
    $entity = entity_load_single($entity_type, $id);
    return entity_label($entity_type, $entity);
  }
}
