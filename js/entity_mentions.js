(function($) {
  var entity_mentions = entity_mentions || {};

  Drupal.behaviors.entity_mentions = {
    attach: function(context, settings) {
      entity_mentions['storage'] = {};

      if (Drupal.wysiwyg != undefined) {
        $.each(Drupal.wysiwyg.editor.init, function(editor) {
          if (typeof entity_mentions[editor] == 'function') {
            entity_mentions[editor](context);
          }
        });
      }

      if (Drupal.settings.ckeditor != undefined && (typeof CKEDITOR !== 'undefined')) {
        entity_mentions.ckeditor(context);
      }

      $.each(Drupal.settings.entity_mentions, function(element, formats) {
        if ($('#' + element + '.entity-mentions').length && !$('#' + element).hasClass('entity-mentions-processed')) {
          entity_mentions.processListeners(element, element, context, false);

          $('#' + element).addClass('entity-mentions-processed');
        }
      });
    }
  };

  entity_mentions.processListeners = function(element, element_target, context, content_editable) {
    if ($('#' + element + '.entity-mentions').length) {
      $.each(Drupal.settings.entity_mentions[element], function(format) {
        var listener = {};
        listener['at'] = '@';
        listener['displayTpl'] = "<li>${selector}</li>";
        listener['insertTpl'] = "${content}";
        listener['searchKey'] = "selector";
        listener['callbacks'] = {};
        listener['callbacks']['beforeInsert'] = function(value, $li) {
          if (content_editable) {
            return $('<span></span>')
              .attr('contenteditable', false)
              .attr('data-mention', value.slice(2))
              .html($li.text())
              .addClass('entity-mention')[0]
              .outerHTML;
          }
          return value;
        };
        listener['callbacks']['remoteFilter'] = function(query, callback) {
          var key = query;

          if (key.length >= 1) {
            if (typeof entity_mentions['storage'][format + '::' + key] === 'undefined') {
              $.get(Drupal.settings.basePath + '?q=entity_mentions/' + format + '/' + key, function(response) {
                entity_mentions['storage'][format + '::' + key] = response;
                callback(response);
              });
            }
            else {
              callback(entity_mentions['storage'][format + '::' + key]);
            }
          }
        };

        if (content_editable) {
          $(element_target, context).atwho(listener);
        }
        else {
          $('#' + element_target, context).atwho(listener);
        }
      });
    }
  }

  /**
   * Integrate with ckEditor.
   */
  entity_mentions.ckeditor = function(context) {
    var onlyOnce = false;
    if (!onlyOnce) {
      onlyOnce = true;
      CKEDITOR.on('instanceCreated', function(e) {
        /**
         * On setData we look for anything that matches the pattern of a mention
         * and aim to replace it with a span that we then get rid of later.
         */
        e.editor.on('setData', function(evt) {
          var format = evt.editor.element.getParent().getParent().getParent().findOne('div.format-toggle select.filter-list').getValue();
          var entity_types = (typeof Drupal.settings.entity_mentions[e.editor.name][format] !== 'undefined') ? Drupal.settings.entity_mentions[e.editor.name][format].entity_types : [];
          var pattern = new RegExp('\@M\\{('+entity_types.join('|')+'):(\\d+)\\}', 'img');
          var mentions = {};
          $.each(entity_types, function() {
            mentions[this] = {};
          });
          evt.data.dataValue = evt.data.dataValue.replace(pattern, function(match, entity_type, id) {
            mentions[entity_type][id] = id;
            return '<span contenteditable="false" class="entity-mention '+entity_type+'-mention" data-mention="'+match.slice(2)+'" data-entity-type="'+entity_type+'" data-entity-id="'+id+'">Mentioned '+entity_type.charAt(0).toUpperCase()+entity_type.slice(1)+'</span>';
          });

          if (Object.getOwnPropertyNames(mentions).length) {
            $.post(Drupal.settings.basePath + '?q=entity_mentions/' + format + '/entity_labels', JSON.stringify(mentions), function (response) {
              $.each(e.editor.document.find('span.entity-mention').$, function () {
                if (typeof response[this.dataset.mention] != 'undefined') {
                  this.innerHTML = response[this.dataset.mention];
                }
              });
            });
          }
        });
        e.editor.on('getData', function(evt) {
          var $data = $('<div>'+evt.data.dataValue+'</div>');
          $data.find('span.atwho-inserted').replaceWith(function() {
            return $(this).html();
          });
          $data.find('span.entity-mention').replaceWith(function() {
            return "@M"+$(this).attr('data-mention');
          });
          evt.data.dataValue = $data.html();
        });
      });

      CKEDITOR.on('instanceReady', function(e) {
        var editor = $('#' + e.editor.name + '.entity-mentions');
        if (editor.length == 1) {
          entity_mentions.processListeners(e.editor.name, e.editor.document.$.body, context, true);
        }
      });
    }
  }

  /**
   * Integrate with TinyMCE.
   */
  entity_mentions.tinymce = function(context) {
    var onlyOnce = false;
    if (!onlyOnce) {
      onlyOnce = true;
      tinyMCE.onAddEditor.add(function(mgr, ed) {
        var editor = $('#' + ed.editorId + '.entity-mentions');
        if (editor.length == 1) {
          ed.onInit.add(function(ed, l) {
            entity_mentions.processListeners(ed.editorId, ed.contentDocument.activeElement, context, true);
          });
        }
      });
    }
  }

})(jQuery);
