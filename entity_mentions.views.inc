<?php
/**
 * @file
 *   Contains Views Hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function entity_mentions_views_data_alter(&$data) {

  // Basic Entity Mentions info.
  $data['entity_mentions']['table']['group'] = t('Entity Mentions');
  $data['entity_mentions']['entity_type'] = array(
    'title' => t('Mentioning Entity Type'),
    'field' => array(
      'label' => t('Mentioning Entity Type'),
      'help' => t('The entity type of the mentioning entity.'),
      'handler' => 'views_handler_field',
    ),
    /** @todo
    'filter' => array(
      'label' => t('Mentioning Entity Type'),
      'help' => t('The entity type of the mentioning entity.'),
      'handler' => '??',
    ),
    **/
  );
  $data['entity_mentions']['entity_id'] = array(
    'title' => t('Mentioning Entity Id'),
    'field' => array(
      'label' => t('Mentioning Entity'),
      'help' => t('The mentioning entity.'),
      'handler' => 'entity_mentions_views_handler_field_entity',
    ),
    /** @todo
    'filter' => array(
      'label' => t('Mentioning Entity Id'),
      'help' => t('The entity id of the mentioning entity.'),
      'handler' => '??',
    ),
    **/
  );

  $data['entity_mentions']['mentioned_type'] = array(
    'title' => t('Mentioned Entity Type'),
    'field' => array(
      'label' => t('Mentioned Entity Type'),
      'help' => t('The entity type of the mentioned entity.'),
      'handler' => 'views_handler_field',
    ),
    /** @todo
    'filter' => array(
      'label' => t('Mentioned Entity Type'),
      'help' => t('The entity type of the mentioned entity.'),
      'handler' => '??',
    ),
    **/
  );
  $data['entity_mentions']['mentioned_id'] = array(
    'title' => t('Mentioned Entity Id'),
    'field' => array(
      'label' => t('Mentioned Entity'),
      'help' => t('The mentioned entity.'),
      'handler' => 'entity_mentions_views_handler_field_mentioned',
    ),
    /** @todo
    'filter' => array(
      'label' => t('Mentioned Entity Id'),
      'help' => t('The entity id of the mentioned entity.'),
      'handler' => '??',
    ),
    **/
  );

  // Relationships.
  foreach (entity_get_info() as $entity_type => $info) {
    if (empty($data[$info['base table']])) {
      continue;
    }

    //  Mentions -> Mentioning Entity
    $mentioning_label = t('Mentioning !entity-type', array(
        '!entity-type' => $info['label'],
      ));
    $data['entity_mentions']['entity_id_'.$entity_type] = array(
      'title' => $mentioning_label,
      'real field' => 'entity_id',
      'relationship' => array(
        'label' => $mentioning_label,
        'help' => t('Relate to the mentioning entities of these mentions.'),
        'base' => $info['base table'],
        'base field' => $info['entity keys']['id'],
        'extra' => array(
          array(
            'table' => 'entity_mentions',
            'field' => 'entity_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );

    //  Mentions -> Mentioned Entity.
    $mentioned_label = t('Mentioned !entity-type', array(
        '!entity-type' => $info['label'],
      ));
    $data['entity_mentions']['mentioned_id_'.$entity_type] = array(
      'title' => $mentioned_label,
      'real field' => 'mentioned_id',
      'relationship' => array(
        'label' => $mentioned_label,
        'help' => t('Relate to the mentioned entities of these mentions.'),
        'base' => $info['base table'],
        'base field' => $info['entity keys']['id'],
        'extra' => array(
          array(
            'table' => 'entity_mentions',
            'field' => 'mentioned_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );

    // Mentioning Entity -> Mentions
    $label = t('Mentions where this !entity-type is mentioning.', array(
      '!entity-type' => $info['label'],
    ));
    $data[$info['base table']]['entity_mentions_mentioning'] = array(
      'title' => $label,
      'real field' => $info['entity keys']['id'],
      'relationship' => array(
        'label' => $label,
        'help' => t('Relate to mentions where this entity is doing the mentioning.'),
        'base' => 'entity_mentions',
        'base field' => 'entity_id',
        'extra' => array(
          array(
            'field' => 'entity_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );

    // Mentioned Entity -> Mentions
    $label = t('Mentions where this !entity-type is being mentioned.', array(
      '!entity-type' => $info['label'],
    ));
    $data[$info['base table']]['entity_mentions_mentioned'] = array(
      'title' => $label,
      'real field' => $info['entity keys']['id'],
      'relationship' => array(
        'label' => $label,
        'help' => t('Relate to mentions where this entity is being mentioned.'),
        'base' => 'entity_mentions',
        'base field' => 'mentioned_id',
        'extra' => array(
          array(
            'field' => 'mentioned_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );
  }
}
