<?php
/**
 * @file
 *   Code for mentions.
 */

/**
 * Implements hook_rules_event_info().
 */
function entity_mentions_rules_event_info() {
  $items = array();
  $items['entity_is_mentioned'] = array(
    'label' => t('When an Entity is Mentioned'),
    'group' => t('Entity'),
    'variables' => array(
      'mentioned_entity' => array(
        'type' => 'entity',
        'label' => t('mentioned entity'),
      ),
      'mentioning_entity' => array(
        'type' => 'entity',
        'label' => t('mentioning entity'),
      ),
      'field_name' => array(
        'type' => 'text',
        'label' => t('field name'),
      ),
      'delta' => array(
        'type' => 'integer',
        'label' => t('delta'),
      ),
    ),
  );

  return $items;
}
